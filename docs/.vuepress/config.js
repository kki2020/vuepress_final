module.exports = {
  title: 'GT',
  description: '금자탑',
  themeConfig: {
    nav: [
      { text: 'Main', link: '/' },
      { text: 'DEVEL', link: '/about/' },
      { text: 'LOG', link: '/contact/' }
    ],
    sidebar: [
      ['/', 'Main'],
      ['/about/', 'DEVEL'],
      ['/contact/', 'LOG']
    ]
  }
}
